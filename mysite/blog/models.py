from django.db import models

from django.utils import timezone
# Create your models here.

class Car(models.Model):
    modelo = models.CharField(max_length=10)
    marca = models.CharField(max_length=10)
    name = models.CharField(max_length=10)
    year_f = models.IntegerField()
    year_m = models.IntegerField()

    def __str__(self):
        return self.name
