price_new_car = int(0)
depreciation = []
support = []
simulation_time = int(raw_input())
price_new_car = float(raw_input())

dp = []
for i in xrange(simulation_time):
	dp.append([])
	for j in xrange(simulation_time):
		dp[i].append(-1)

for i in range(0,simulation_time) :
    depreciation.append( float(raw_input()) )
    pass
for i in range(0,simulation_time):
    support.append( float(raw_input()) )
    pass

def changecar( price_actual_car , year , old ):
	if( year == simulation_time ): 
		return 0
	elif( int(dp[year][old]) != -1 ):
		return dp[year][old]
	else:
		change_car = changecar( price_new_car, year+1, 0 ) + support[0] + price_new_car - price_actual_car
		keep_car = changecar( price_actual_car - depreciation[old] , year+1 , old + 1 ) + support[old]
		dp[year][old] = float(min(change_car,keep_car))
		return dp[year][old]

print changecar(price_new_car,0,0)